//
//  BLETableViewController.swift
//  BuddySwitch
//
//  Created by Dylan Baroody on 7/24/17.
//  Copyright © 2017 Dylan Baroody. All rights reserved.
//

import UIKit
import os.log
import CoreBluetooth

class BLETableViewController: UITableViewController, LeDiscoveryDelegate, LeBuddyButtonProtocol {
    func buttonServiceDidInitialize(_ service: LeBuddyButtonService!) {
        return
    }

    
    //MARK: Properties
    
    var devices = [BLEDevice]()
    var identifier: UUID?
    var connectedDevices: NSMutableArray = []
    var discovery: LeDiscovery = LeDiscovery.sharedInstance() as! LeDiscovery
    var connect = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set up discovery object
        discovery.discoveryDelegate = self as LeDiscoveryDelegate
        discovery.peripheralDelegate = self as LeBuddyButtonProtocol
        discovery.startScanning(forUUIDString: kHIDUUIDString)
        
        // Load a previously paired device
        identifier = loadIdentifier()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        discovery.stopScanning()
        
        // Save paired device
        saveIdentifier()
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devices.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "BLETableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as?  BLETableViewCell else {
            fatalError("The dequeued cell is not an instance of BLETableViewCell.")
        }
        
        // Fetches the appropriate device for the data source layout.
        let device = devices[indexPath.row]
        
        cell.nameLabel.text = device.name
        cell.photoImageView.image = device.photo
        print(device.active)
        if device.active {
            cell.activeLabel.text = "Active Device"
            cell.activeLabel.textColor = UIColor.green
        } else {
            cell.activeLabel.text = "Not Active"
            cell.activeLabel.textColor = UIColor.black
        }
        
        return cell
    }
    
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
            
        case "ShowDetails":
            guard let deviceDetailViewController = segue.destination as? DeviceTableViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            guard let selectedMealCell = sender as? BLETableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedMealCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            // Save the identifier and connect to the selected device
            let selectedDevice = devices[indexPath.row]
            deviceDetailViewController.device = selectedDevice
            identifier = selectedDevice.identifier
            saveIdentifier()
        
            discovery.connect(selectedDevice.peripheral)
            deviceDetailViewController.discovery = discovery
            
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
            
        }
    }
    
    
    //MARK: BuddyButtonService Delegate Methods
    func buttonServiceDidChangeStatus(_ service: LeBuddyButtonService) {
        if (service.peripheral.state == CBPeripheralState.connected) {
            if (!connectedDevices.contains(service)) {
                connectedDevices.add(service)
            }
        } else {
            if (connectedDevices.contains(service)) {
                connectedDevices.remove(service)
            }
        }
    }
    
    func activeDeviceDidChange(_ service: LeBuddyButtonService) {
         return
    }
    
    func deviceListDidChange(_ service: LeBuddyButtonService!) {
        return
    }
    
    func devCountDidChange(_ service: LeBuddyButtonService!) {
        return
    }
    
    func setupModeDidChange(_ service: LeBuddyButtonService!) {
        return
    }
    
    func buttonServiceDidReset() {
        connectedDevices.removeAllObjects()
    }
    
    //MARK: LeDiscoveryDelegate Methods
    func discoveryDidRefresh() {
        loadDeviceFromBLE()
        tableView.reloadData()
    }
    
    func discoveryStatePoweredOff() {
        let title: String = "Bluetooth Power"
        let message: String = "You must turn on Bluetooth in Settings in order to use LE"
        let alertController = UIAlertController(title: title, message: message, preferredStyle:UIAlertControllerStyle.alert)
        
        present(alertController, animated: true, completion: nil)
    }
    
    //MARK: Actions
    
    @IBAction func unwindToBLEList(sender: UIStoryboardSegue) {
        discovery.discoveryDelegate = self as LeDiscoveryDelegate
        discovery.peripheralDelegate = self as LeBuddyButtonProtocol
        
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            devices[selectedIndexPath.row].identifier = identifier
            saveDevices()
            discovery.disconnectPeripheral(devices[selectedIndexPath.row].peripheral)
        }
    }
    
    //MARK: Private Methods
    
    private func saveIdentifier() {
        
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(identifier!, toFile: BLEDevice.ArchiveURL.path)
        
        if isSuccessfulSave {
            os_log("Identifier successfully saved.", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save Devices...", log: OSLog.default, type: .error)
        }
        
    }
    
    private func loadIdentifier() -> UUID? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: BLEDevice.ArchiveURL.path) as? UUID
    }
    
    private func saveDevices() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(devices, toFile: BLEDevice.ArchiveURL.path)
        
        if isSuccessfulSave {
            os_log("Devices successfully saved.", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save meals...", log: OSLog.default, type: .error)
        }
        
    }
    
    private func loadDevices() -> [BLEDevice]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: BLEDevice.ArchiveURL.path) as? [BLEDevice]
    }
    
    private func loadDeviceFromBLE() {
        devices = [BLEDevice]()
        
        // Retrieve previously paired device
        discovery.retrieve(identifier)
        let deviced = discovery.foundPeripherals
        if deviced?.count == 0 {
            os_log("No Connected Devices")
        }
        
        var devNames: [String]?
        var i = 0
        var j = 0
        
        
        // Retrieve found peripherals and store in devices
        while i < (deviced?.count)! {
            let peripheral = (deviced?[i] as! CBPeripheral)
            let buttons = loadDevices()
            if buttons != nil {
                while j < (buttons?.count)! {
                    // Check stored name, load names of buddy button devices
                    if buttons?[j].name == peripheral.name {
                        devNames = buttons?[j].devNames
                    } else {
                        devNames = nil
                    }
                    j += 1
                }
            }
            identifier = peripheral.identifier
            guard let device = BLEDevice(name: peripheral.name!, photo: #imageLiteral(resourceName: "phone"), active: false, devNames: devNames, identifier:  identifier) else {
                fatalError("Unable to instantiate device")
            }
            device.peripheral = peripheral
            devices += [device]
            i += 1
        }
    }
    
}
