//
//  BLEDevice.swift
//  BuddySwitch
//
//  Created by Dylan Baroody on 7/24/17.
//  Copyright © 2017 Dylan Baroody. All rights reserved.
//

import UIKit
import CoreBluetooth
import os.log

class BLEDevice: NSObject, NSCoding {
    
    //MARK: Properties
    
    var name: String
    var photo: UIImage?
    var active: Bool
    var devNames: [String]?
    var identifier: UUID?
    var peripheral: CBPeripheral?
    
    //MARK: Archiving Paths
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("devices")
    
    //MARK: Types
    
    struct PropertyKey {
        static let name = "name"
        static let photo = "photo"
        static let active = "active"
        static let devNames = "devNames"
        static let identifier = "identifier"
    }
    
    //MARK: Initialization
    init?(name: String, photo: UIImage?, active: Bool, devNames: [String]?, identifier: UUID?) {
        
        // The name must not be empty
        guard !name.isEmpty else {
            return nil
        }
        
        // Initialize stored properties.
        self.name = name
        self.photo = photo
        self.active = active
        self.devNames = devNames
        self.identifier = identifier
    }
    
    //MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: PropertyKey.name)
        aCoder.encode(photo, forKey: PropertyKey.photo)
        aCoder.encode(active, forKey: PropertyKey.active)
        aCoder.encode(devNames, forKey: PropertyKey.devNames)
        aCoder.encode(devNames, forKey: PropertyKey.devNames)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        // The name is required. If we cannot decode a name string, the initializer should fail.
        guard let name = aDecoder.decodeObject(forKey: PropertyKey.name) as? String else {
            os_log("Unable to decode the name for a Device object.", log: OSLog.default, type: .debug)
            return nil
        }
        
        // Because photo is an optional property of Meal, just use conditional cast.
        let photo = aDecoder.decodeObject(forKey: PropertyKey.photo) as? UIImage
        
        let active = aDecoder.decodeBool(forKey: PropertyKey.active)
        
        let devNames = aDecoder.decodeObject(forKey: PropertyKey.devNames) as? [String]?
        
        let identifier = aDecoder.decodeObject(forKey: PropertyKey.identifier) as? UUID?
        
        // Must call designated initializer.
        self.init(name: name, photo: photo, active: active, devNames: devNames!, identifier: identifier!)
    }
}
