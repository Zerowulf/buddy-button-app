//
//  DeviceTableViewController.swift
//  BuddySwitch
//
//  Created by Dylan Baroody on 7/24/17.
//  Copyright © 2017 Dylan Baroody. All rights reserved.
//

import UIKit
import os.log
import CoreBluetooth

class DeviceTableViewController: UITableViewController, LeBuddyButtonProtocol {
    
    //MARK: Properties
    var device: BLEDevice?
    var devices = [Device]()
    var discovery: LeDiscovery?
    var service: LeBuddyButtonService?
    var active: UInt32?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        discovery?.peripheralDelegate = self as LeBuddyButtonProtocol
        
        loadDeviceFromBLE()
    }
    
    
    override func didReceiveMemoryWarning() {
        saveDevices()
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devices.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "DeviceTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as?  DeviceTableViewCell else {
            fatalError("The dequeued cell is not an instance of DeviceTableViewCell.")
        }
        
        // Fetches the appropriate meal for the data source layout.
        let device = devices[indexPath.row]
        
        cell.nameLabel.text = device.name
        cell.photoImageView.image = device.photo
        if device.active {
            cell.activeLabel.text = "Active Device"
            cell.activeLabel.textColor = UIColor.green
            active = UInt32(indexPath.row)
        } else {
            cell.activeLabel.text = "Not Active"
            cell.activeLabel.textColor = UIColor.black
        }
        
        return cell
    }
    
    
    
    //    // Override to support conditional editing of the table view.
    //    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    //        // Return false if you do not want the specified item to be editable.
    //        return true
    //    }
    //
    //
    //
    //    // Override to support editing the table view.
    //    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    //        if editingStyle == .delete {
    //            devices.remove(at: indexPath.row)
    //            saveDevices()
    //            tableView.deleteRows(at: [indexPath], with: .fade)
    //        } else if editingStyle == .insert {
    //            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    //        }
    //
    //
    //    }
    
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
        case "AddItem":
            os_log("Adding a new device.", log: OSLog.default, type: .debug)
            
        case "ShowDetail":
            guard let deviceDetailViewController = segue.destination as? DeviceViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            guard let selectedMealCell = sender as? DeviceTableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedMealCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selectedDevice = devices[indexPath.row]
            deviceDetailViewController.device = selectedDevice
            
        case "Unwind":
            service?.writeActiveDevice(UInt8(active!))
            break
            
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
            
        }
    }
    
    
    //MARK: Actions
    
    @IBAction func unwindToMealList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? DeviceViewController, let devicez = sourceViewController.device {
            
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                if devicez.active {
                    for devicex in devices {
                        if devicex.active {
                            devicex.active = false
                        }
                        
                    }
                    active = UInt32(selectedIndexPath.row)
                    tableView.reloadData()
                }
                
                // Update an existing device.
                device?.devNames?[selectedIndexPath.row] = devicez.name
                devices[selectedIndexPath.row] = devicez
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            } else {
                
                // Add a new device.
                let newIndexPath = IndexPath(row: devices.count, section: 0)
                
                devices.append(devicez)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
                if devicez.active {
                    for devicex in devices {
                        if devicex.active {
                            devicex.active = false
                        }
                        
                    }
                    devicez.active = true
                    tableView.reloadData()
                }
            }
            
            // Save the devices.
            saveDevices()
        }
    }
    
    
    //MARK: BuddyButtonService Delegate Methods
    func buttonServiceDidChangeStatus(_ service: LeBuddyButtonService) {
        return
    }
    
    func buttonServiceDidInitialize(_ service: LeBuddyButtonService!) {
        loadDeviceFromBLE()
        tableView.reloadData()
    }
    
    func activeDeviceDidChange(_ service: LeBuddyButtonService) {
    }
    
    func deviceListDidChange(_ service: LeBuddyButtonService!) {
    }
    
    func devCountDidChange(_ service: LeBuddyButtonService!) {
    }
    
    func setupModeDidChange(_ service: LeBuddyButtonService!) {
    }
    
    func buttonServiceDidReset() {
        return
    }
    
    //MARK: Private Methods
    
    private func loadSampleDevices() {
        
        guard let device1 = Device(name: "My Computer", photo: #imageLiteral(resourceName: "computer"), rating: 4, active: true, id: 0) else {
            fatalError("Unable to instantiate device1")
        }
        
        guard let device2 = Device(name: "My Phone", photo: #imageLiteral(resourceName: "phone"), rating: 5, active: false, id: 1) else {
            fatalError("Unable to instantiate device2")
        }
        
        guard let device3 = Device(name: "Work Computer", photo: #imageLiteral(resourceName: "computer"), rating: 3, active: false, id: 2) else {
            fatalError("Unable to instantiate device3")
        }
        devices += [device1, device2, device3]
        
    }
    
    private func saveDevices() {
        
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(devices, toFile: Device.ArchiveURL.path)
        
        if isSuccessfulSave {
            os_log("Services successfully saved.", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save meals...", log: OSLog.default, type: .error)
        }
        
    }
    
    private func loadDevices() -> [Device]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Device.ArchiveURL.path) as? [Device]
    }
    
    private func loadDeviceFromBLE() {
        var active: Bool
        var name: String
        let deviced = discovery?.connectedServices
        if deviced?.count == 0 {
            os_log("No Connected Services")
            return
        }
        // Retrieve devCount and activeDev from characteristics
        service = (deviced?[0] as! LeBuddyButtonService)
        let devCount = service?.devCount
        let activeDev = service?.activeDevice
        
        // Display all stored devices on buddy button
        var i = 0
        while UInt32(i) < devCount! {
            let id = UInt32(i)
            if id == activeDev {
                active = true
            } else {
                active = false
            }
            name = "Device \(i)"
            // Deal with attempting to restore stored names
            if device?.devNames != nil {
                if  (device?.devNames?.count)! < i {
                    name = (device?.devNames?[i])!
                } else {
                    device?.devNames?.append(name)
                }
            } else {
                if device?.devNames == nil {
                    device?.devNames = []
                }
                device?.devNames?.append(name)
            }
            
            guard let devicex = Device(name: name, photo: #imageLiteral(resourceName: "phone"), rating: 0, active: active, id: Int(id)) else {
                fatalError("Unable to instantiate device")
            }
            
            devices += [devicex]
            i += 1
        }
        
    }
}


