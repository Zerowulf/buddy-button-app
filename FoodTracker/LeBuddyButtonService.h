//
//  LeBuddyButtonService.h
//  BuddySwitch
//
//  Created by Dylan Baroody on 7/26/17.
//  Copyright © 2017 Dylan Baroody. All rights reserved.
//

#ifndef LeBuddyButtonService_h
#define LeBuddyButtonService_h

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

/****************************************************************************/
/*						Service Characteristics								*/
/****************************************************************************/
extern NSString *kHIDUUIDString;
extern NSString *kBuddyButtonServiceUUIDString;                 // DEADF154-0000-0000-0000-0000DEADF154     Service UUID
extern NSString *kActiveDeviceUUIDString;                       // CCCCFFFF-DEAD-F154-1319-740381000000     Active Device Characteristic
extern NSString *kDeviceListUUIDString;                         // C0C0C0C0-DEAD-F154-1319-740381000000     Device List Characteristic
extern NSString *kSetupModeUUIDString;                          // EDEDEDED-DEAD-F154-1319-740381000000     Setup Mode Characteristic

extern NSString *kAlarmServiceEnteredBackgroundNotification;
extern NSString *kAlarmServiceEnteredForegroundNotification;

/****************************************************************************/
/*								Protocol									*/
/****************************************************************************/
@class LeBuddyButtonService;

typedef enum {
    kAlarmConnect  = 0,
    kAlarmDisconnect   = 1,
} AlarmType;

@protocol LeBuddyButtonProtocol<NSObject>
- (void) buttonServiceDidInitialize:(LeBuddyButtonService*)service;
- (void) buttonServiceDidChangeStatus:(LeBuddyButtonService*)service;
- (void) activeDeviceDidChange:(LeBuddyButtonService*)service;
- (void) deviceListDidChange:(LeBuddyButtonService*)service;
- (void) devCountDidChange:(LeBuddyButtonService*)service;
- (void) setupModeDidChange:(LeBuddyButtonService*)service;
- (void) buttonServiceDidReset;
@end

/****************************************************************************/
/*						BuddyButton service.                                */
/****************************************************************************/
@interface LeBuddyButtonService : NSObject

- (id) initWithPeripheral:(CBPeripheral *)peripheral controller:(id<LeBuddyButtonProtocol>)controller;
- (void) reset;
- (void) start;

/* Querying Sensor */
@property (readonly) uint32_t activeDevice;
@property (readonly) uint32_t *deviceList;
@property (readonly) uint32_t devCount;
@property (readonly) bool setupMode;

/* Control the active device */
- (void) writeActiveDevice:(uint8_t)ad;
- (void) writeSetupMode:(BOOL)setup;

@property (readonly) CBPeripheral *peripheral;
@end



#endif /* LeBuddyButtonService_h */
