//
//  LeBuddyButtonService.m
//  BuddySwitch
//
//  Created by Dylan Baroody on 7/26/17.
//  Copyright © 2017 Dylan Baroody. All rights reserved.
//

#import "LeBuddyButtonService.h"
#import "LeDiscovery.h"

NSString *kHIDUUIDString                = @"00001812-0000-1000-8000-00805F9B34FB";
NSString *kBuddyButtonServiceUUIDString = @"DB250001-2397-46ED-9DA0-9A20A150F2D2";
NSString *kActiveDeviceUUIDString       = @"DB250003-2397-46ED-9DA0-9A20A150F2D2";
NSString *kDeviceListUUIDString         = @"DB250002-2397-46ED-9DA0-9A20A150F2D2";
NSString *kSetupModeUUIDString          = @"DB250004-2397-46ED-9DA0-9A20A150F2D2";
NSString *kDevCountUUIDString           = @"DB250005-2397-46ED-9DA0-9A20A150F2D2";

NSString *kAlarmServiceEnteredBackgroundNotification = @"kAlarmServiceEnteredBackgroundNotification";
NSString *kAlarmServiceEnteredForegroundNotification = @"kAlarmServiceEnteredForegroundNotification";

@interface LeBuddyButtonService() <CBPeripheralDelegate> {
@private
    CBPeripheral		*servicePeripheral;
    
    CBService			*buddyButtonService;
    
    CBCharacteristic    *activeDeviceCharacteristic;
    CBCharacteristic	*deviceListCharacteristic;
    CBCharacteristic    *setupModeCharacteristic;
    CBCharacteristic    *devCountCharacteristic;
    
    CBUUID              *deviceListUUID;
    CBUUID              *setupModeUUID;
    CBUUID              *activeDeviceUUID;
    CBUUID              *devCountUUID;
    
    id<LeBuddyButtonProtocol>	peripheralDelegate;
}
@end

@implementation LeBuddyButtonService


@synthesize peripheral = servicePeripheral;


#pragma mark -
#pragma mark Init
/****************************************************************************/
/*								Init										*/
/****************************************************************************/
- (id) initWithPeripheral:(CBPeripheral *)peripheral controller:(id<LeBuddyButtonProtocol>)controller
{
    self = [super init];
    if (self) {
        servicePeripheral = [peripheral retain];
        [servicePeripheral setDelegate:self];
        peripheralDelegate = controller;
        
        deviceListUUID	= [[CBUUID UUIDWithString:kDeviceListUUIDString] retain];
        activeDeviceUUID	= [[CBUUID UUIDWithString:kActiveDeviceUUIDString] retain];
        setupModeUUID	= [[CBUUID UUIDWithString:kSetupModeUUIDString] retain];
        devCountUUID	= [[CBUUID UUIDWithString:kDevCountUUIDString] retain];
    }
    return self;
}


- (void) dealloc {
    if (servicePeripheral) {
        [servicePeripheral setDelegate:[LeDiscovery sharedInstance]];
        [servicePeripheral release];
        servicePeripheral = nil;
        
        [deviceListUUID release];
        [activeDeviceUUID release];
        [setupModeUUID release];
    }
    [super dealloc];
}


- (void) reset
{
    if (servicePeripheral) {
        [servicePeripheral release];
        servicePeripheral = nil;
    }
}

#pragma mark -
#pragma mark Service interaction
/****************************************************************************/
/*							Service Interactions							*/
/****************************************************************************/
- (void) start
{
    CBUUID	*serviceUUID	= [CBUUID UUIDWithString:kBuddyButtonServiceUUIDString];
    NSArray	*serviceArray	= [NSArray arrayWithObjects:serviceUUID, nil];
    
    [servicePeripheral discoverServices:serviceArray];
}

- (void) peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    NSArray		*services	= nil;
    NSArray		*uuids	= [NSArray arrayWithObjects:deviceListUUID, // Device List
                           activeDeviceUUID, // Active  Device
                           setupModeUUID, // Setup Mode
                           devCountUUID, // Device Count
                           nil];
    
    if (peripheral != servicePeripheral) {
        NSLog(@"Wrong Peripheral.\n");
        return ;
    }
    
    if (error != nil) {
        NSLog(@"Error %@\n", error);
        return ;
    }
    
    services = [peripheral services];
    if (!services || ![services count]) {
        return ;
    }
    
    buddyButtonService = nil;
    
    for (CBService *service in services) {
        if ([[service UUID] isEqual:[CBUUID UUIDWithString:kBuddyButtonServiceUUIDString]]) {
            buddyButtonService = service;
            break;
        }
    }
    
    if (buddyButtonService) {
        [peripheral discoverCharacteristics:uuids forService:buddyButtonService];
    }
}

- (void) peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error;
{
    NSArray		*characteristics	= [service characteristics];
    CBCharacteristic *characteristic;
    
    if (peripheral != servicePeripheral) {
        NSLog(@"Wrong Peripheral.\n");
        return ;
    }
    
    if (service != buddyButtonService) {
        NSLog(@"Wrong Service.\n");
        return ;
    }
    
    if (error != nil) {
        NSLog(@"Error %@\n", error);
        return ;
    }
    
    for (characteristic in characteristics) {
        NSLog(@"discovered characteristic %@", [characteristic UUID]);
        
        if ([[characteristic UUID] isEqual:activeDeviceUUID]) { // Active Device
            NSLog(@"Discovered Active Device Characteristic");
            activeDeviceCharacteristic = [characteristic retain];
            [peripheral readValueForCharacteristic:characteristic];
        }
        else if ([[characteristic UUID] isEqual:deviceListUUID]) { // Device List
            NSLog(@"Discovered Device List Characteristic");
            deviceListCharacteristic = [characteristic retain];
            [peripheral readValueForCharacteristic:characteristic];
        }
        else if ([[characteristic UUID] isEqual:setupModeUUID]) { // Setup Mode
            NSLog(@"Discovered Setup Mode Characteristic");
            setupModeCharacteristic = [characteristic retain];
            [peripheral readValueForCharacteristic:characteristic];
        }
        else if ([[characteristic UUID] isEqual:devCountUUID]) { // Device Count
            NSLog(@"Discovered Device Count Characteristic");
            devCountCharacteristic = [characteristic retain];
            [peripheral readValueForCharacteristic:characteristic];
        }
    }
    [peripheralDelegate buttonServiceDidInitialize:((LeBuddyButtonService *)buddyButtonService)];
}

#pragma mark -
#pragma mark Characteristics interaction
/****************************************************************************/
/*						Characteristics Interactions						*/
/****************************************************************************/
- (void) writeActiveDevice:(uint8_t)ad
{
    NSData  *data	= nil;
    
    if (!servicePeripheral) {
        NSLog(@"Not connected to a peripheral");
        return ;
    }
    
    if (!activeDeviceCharacteristic) {
        NSLog(@"No valid minTemp characteristic");
        return;
    }
    
    data = [NSData dataWithBytes:&ad length:sizeof (ad)];
    [servicePeripheral writeValue:data forCharacteristic:activeDeviceCharacteristic type:CBCharacteristicWriteWithResponse];
    NSLog(@"Wrote characteristic");
}

- (void) writeSetupMode:(BOOL)setup
{
    NSData  *data  = nil;
    
    if (!servicePeripheral) {
        NSLog(@"Not connected to a peripheral");
        return ;
    }
    
    if (!activeDeviceCharacteristic) {
        NSLog(@"No valid minTemp characteristic");
        return;
    }
    
    data = [NSData dataWithBytes:&setup length:sizeof (setup)];
    [servicePeripheral writeValue:data forCharacteristic:setupModeCharacteristic type:CBCharacteristicWriteWithResponse];
}

- (void) peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    
    if (peripheral != servicePeripheral) {
        NSLog(@"Wrong peripheral\n");
        return ;
    }
    
    if ([error code] != 0) {
        NSLog(@"Error %@\n", error);
        return ;
    }
    
    /* Active Device change */
    if ([[characteristic UUID] isEqual:activeDeviceUUID]) {
        [peripheralDelegate activeDeviceDidChange:self];
        return;
    }
    
    /* Device List change */
    if ([[characteristic UUID] isEqual:deviceListUUID]) {
        [peripheralDelegate deviceListDidChange:self];
        return;
    }
    
    /* Setup Mode change */
    if ([[characteristic UUID] isEqual:setupModeUUID]) {
        [peripheralDelegate setupModeDidChange:self];
        return;
    }
    
    /* Device Count change */
    if ([[characteristic UUID] isEqual:devCountUUID]) {
        [peripheralDelegate devCountDidChange:self];
        return;
    }
}

- (void) peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    NSLog(@" error => %@", [error userInfo]);
    /* When a write occurs, need to set off a re-read of the local CBCharacteristic to update its value */
    [peripheral readValueForCharacteristic:characteristic];
    
    /* Active Device change */
    if ([[characteristic UUID] isEqual:activeDeviceUUID]) {
        [peripheralDelegate activeDeviceDidChange:self];
        return;
    }
    
    /* Setup Mode change */
    if ([[characteristic UUID] isEqual:setupModeUUID]) {
        [peripheralDelegate setupModeDidChange:self];
        return;
    }
}
- (uint32_t) activeDevice
{
    uint32_t value = 0;
    
    if (activeDeviceCharacteristic) {
        [[activeDeviceCharacteristic value] getBytes:&value length:sizeof (value)];
    }
    return value;
}

- (uint32_t) devCount
{
    uint32_t value	= 0;
    
    if (devCountCharacteristic) {
        [[devCountCharacteristic value] getBytes:&value length:sizeof (value)];
    }
    return value;
}

- (uint32_t*) deviceList
{
    uint32_t* value = NULL;
    if (deviceListCharacteristic) {
        [[deviceListCharacteristic value] getBytes:&value length:(sizeof (uint32_t)) * self.devCount];
    }
    return value;
}

- (bool) setupMode
{
    bool value	= false;
    
    if (setupModeCharacteristic) {
        [[setupModeCharacteristic value] getBytes:&value length:sizeof (value)];
    }
    return value;
}

@end
