//
//  BLETableViewCell.swift
//  BuddySwitch
//
//  Created by Dylan Baroody on 7/24/17.
//  Copyright © 2017 Dylan Baroody. All rights reserved.
//

import UIKit

class BLETableViewCell: UITableViewCell {
    
    //MARK: Properties
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var activeLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
